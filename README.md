# <ins>CME495 Capstone Project</ins>
This project is for people who like beer, and gadgets. This is an attempt to solve a problem that beer brewers often face, how to get the most out of your kegs of beer without having hazy bottoms in your glass. The way to do this is to pour as much of the keg as possible while leaving the sediment (AKA trub) at the bottom of the keg then just dumping that out. The traditional way to see how much beer is left in the keg, and when to dump out the trub, was to just pick up the keg and shake it to see where the level is, but of coure when you shake it you stir up all that nasty sediment. So the purpose of this project is to build an automated way to detect when a beer keg is starting to get low, and when it is time to dump the trub. It is also required to have this all be done remotely up to 30ft for convenience, but we also wanted to make it accessible from anywhere, lets say during a boring meeting, or church, or on the can - we dont judge.

</br></br>

## <ins>Group Members</ins>
* Michael Coquet 
* Ryan Betker
* Layne Forbes
* Nnamdi Emezieke
</br></br>

## <ins>Usage Instructions</ins>
_put usage instructions for user here_
</br></br>

## <ins>Design schematics and Diagrams</ins>
See [project wiki](https://gitlab.com/m.coquet2/cme495_capstone/-/wikis/Home) for detailed design docs
