export interface Keg {
    active: number;
    brew_description: string;
    brew_name: string;
    create_date: string;
    current_level: number;
    finish_date: string;
    batch_id: string;
    kegging_date: string;
    mashing_date: string;
    slot_id: number;
    comment: string;
}
