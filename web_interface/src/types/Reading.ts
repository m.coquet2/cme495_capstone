export interface Reading {
    batch_id: string;
    slot_id: number;
    timestamp: string;
    reading: number;
}
